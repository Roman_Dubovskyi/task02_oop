package Controller;

import View.Menu;

public class Controller {
    private Menu menu = new Menu();

    public void execute() {
        menu.showMenu();
        menu.createMenu();
    }
}
