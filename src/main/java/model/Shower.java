package model;

public class Shower extends Plumbing {
    private static String[] showersNames;

    static {
        showersNames = new String[]{"Smaragd", "Polo", "Rich", "Dark"};
    }

    public static int[] getShowersPrices() {
        return showersPrices;
    }

    private static int[] showersPrices = new int[]{100, 125, 300, 350};
    private static int counter = 0;

    public Shower() {
        setSinksNames();
        setSinksPrices();
    }

    public void setSinksNames() {
        if (counter == showersNames.length) {
            counter = 0;
        }
        setName(showersNames[counter]);
        counter++;
    }

    public void setSinksPrices() {
        setPrice(showersPrices[counter - 1]);
    }

}
