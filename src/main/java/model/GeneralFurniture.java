package model;

public class GeneralFurniture extends Furniture {
    private static String[] furnitureModels = new String[]{"Bed", "Table", "Chair", "Couch"};
    private static int[] furniturePrices = new int[]{120, 40, 35, 150};
    private static int counter = 0;

    public GeneralFurniture() {
        setFurnitureModels();
        setFurniturePrices();
    }

    public void setFurnitureModels() {
        if (counter == furnitureModels.length) counter = 0;
        setModel(furnitureModels[counter]);
        counter++;
    }

    public void setFurniturePrices() {
        setPrice(furniturePrices[counter - 1]);
    }
}
