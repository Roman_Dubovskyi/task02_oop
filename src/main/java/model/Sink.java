package model;

public class Sink extends Plumbing {
    private static String[] sinksNames;

    static {
        sinksNames = new String[]{"Khotler", "Marko", "Ketler", "Sink"};
    }

    private static int[] sinksPrices = new int[]{15, 20, 25, 30};
    private static int counter = 0;

    public Sink() {
        setSinksNames();
        setSinksPrices();
    }

    public void setSinksNames() {
        if (counter == sinksNames.length) {
            counter = 0;
        }
        setName(sinksNames[counter]);
        counter++;
    }

    public void setSinksPrices() {
        setPrice(sinksPrices[counter - 1]);
    }

}
