package model;

public class ExteriorPaint extends Paint {
    private  static String[] PaintsColors = new String[]{"Brown","Yellow","Black","White"};
    private static int[] PaintsPrices = new int[]{5,10,20,25};
    private static int counter = 0;
    public ExteriorPaint(){
        setPaintsColors();
        setPaintsPrices();
    }
    public void setPaintsColors(){
        if(counter ==PaintsColors.length)counter=0;
        setColor(PaintsColors[counter]);
        counter++;
    }
    public void setPaintsPrices(){
        setPrice(PaintsPrices[counter-1]);
    }

}
