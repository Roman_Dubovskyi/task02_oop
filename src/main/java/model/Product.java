package model;

import java.util.ArrayList;

public class Product {
    private ArrayList<Sink> sinks = new ArrayList<Sink>();
    private ArrayList<Shower> showers = new ArrayList<Shower>();
    private ArrayList<ExteriorPaint> exteriorPaints = new ArrayList<ExteriorPaint>();
    private ArrayList<InteriorPaint> interiorPaints = new ArrayList<InteriorPaint>();
    private ArrayList<GeneralFurniture> furnitures = new ArrayList<GeneralFurniture>();

    public Product() {
        setSinks();
        setShowers();
        setExteriorPaints();
        setInteriorPaints();
        setFurniture();
    }

    public void setFurniture() {
        for (int i = 0; i < Plumbing.getSizeOfLists(); i++)
            this.furnitures.add(new GeneralFurniture());
    }

    public void setInteriorPaints() {
        for (int i = 0; i < Plumbing.getSizeOfLists(); i++)
            this.interiorPaints.add(new InteriorPaint());
    }

    public void setShowers() {
        for (int i = 0; i < Plumbing.getSizeOfLists(); i++)
            this.showers.add(new Shower());
    }

    public void setExteriorPaints() {
        for (int i = 0; i < Plumbing.getSizeOfLists(); i++)
            this.exteriorPaints.add(new ExteriorPaint());
    }

    public void setSinks() {
        for (int i = 0; i < Plumbing.getSizeOfLists(); i++)
            this.sinks.add(new Sink());
    }

    public void showExteriorPaints() {
        System.out.println("Exterior Paints: ");
        for (ExteriorPaint exteriorPaint : this.exteriorPaints)
            System.out.println(exteriorPaint.toString());
    }

    public void showInteriorPaints() {
        System.out.println("Interior Paints: ");
        for (InteriorPaint interiorPaint : this.interiorPaints)
            System.out.println(interiorPaint.toString());
    }

    public void showShowers() {
        System.out.println("Showers: ");
        for (Shower shower : this.showers)
            System.out.println(shower.toString());
    }

    public void showSinks() {
        System.out.println("Sinks: ");
        for (Sink sink : this.sinks)
            System.out.println(sink.toString());
    }

    public void showFurniture() {
        System.out.println("Furniture: ");
        for (Furniture furniture : this.furnitures)
            System.out.println(furniture.toString());
    }

    public void showSinksByPrices(int from, int to) {
        System.out.println("Sinks(suitable by price): ");
        for (Sink sink : this.sinks)
            if (sink.getPrice() >= from && sink.getPrice() <= to)
                System.out.println(sink.toString());
    }

    public void showShowersByPrices(int from, int to) {
        System.out.println("Showers(suitable by price): ");
        for (Shower shower : this.showers) {
            if (from > Shower.getShowersPrices()[3] || to < Shower.getShowersPrices()[0]) {
                System.out.println("No items found");
                break;
            } else {
                if (shower.getPrice() >= from && shower.getPrice() <= to)
                    System.out.println(shower.toString());
            }
        }

    }

    public void showInteriorPaintsByPrices(int from, int to) {
        System.out.println("Interior Paints(suitable by price): ");
        for (InteriorPaint paint : this.interiorPaints)
            if (paint.getPrice() >= from && paint.getPrice() <= to)
                System.out.println(paint.toString());

    }

    public void showExteriorPaintsByPrices(int from, int to) {
        System.out.println("Exterior Paints(suitable by price): ");
        for (ExteriorPaint paint : this.exteriorPaints)
            if (paint.getPrice() >= from && paint.getPrice() <= to)
                System.out.println(paint.toString());

    }

    public void showFurnitureByPrices(int from, int to) {
        System.out.println("Furniture(suitable by price): ");
        for (GeneralFurniture furniture : this.furnitures)
            if (furniture.getPrice() >= from && furniture.getPrice() <= to)
                System.out.println(furniture.toString());

    }
}
