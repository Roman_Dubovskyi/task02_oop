package model;

public abstract class Plumbing {
    private String name;
    private int price;
    private static final int SIZE_OF_LISTS = 4;

    public static int getSizeOfLists() {
        return SIZE_OF_LISTS;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                        ", price=" + price;
    }
}
