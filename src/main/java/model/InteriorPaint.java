package model;

public class InteriorPaint extends Paint {
    private static String[] PaintColorIn = new String[]{"Grey(I)", "Yellow(I)", "Blue(I)", "Green(I)"};
    private static int[] PaintPriceIn = new int[]{7, 12, 24, 38};
    private static int counter = 0;

    public InteriorPaint() {
        setPaintsColors();
        setPaintsPrices();
    }

    public void setPaintsColors() {
        if (counter == PaintColorIn.length) counter = 0;
        setColor(PaintColorIn[counter]);
        counter++;
    }

    public void setPaintsPrices() {
        setPrice(PaintPriceIn[counter - 1]);
    }
}
