package View;

import model.Product;

import java.util.Scanner;

public class Menu {

    private Product product = new Product();
    Scanner input = new Scanner(System.in);
    private int from;
    private int to;

    public void showMenu() {
        System.out.println("1.Show all products\n2.Show products by categories\n3.Choose by price");
        System.out.println("4.Exit");
    }

    public void subMenu() {
        System.out.println("1.Back\n2.Exit");
        switch (input.nextInt()) {
            case (1):
                showMenu();
                createMenu();
                break;
            default:
                Runtime.getRuntime().exit(0);
        }
    }

    public void inputIntervals() {
        System.out.println("Enter intervals of prices:from and to");
        this.from = input.nextInt();
        this.to = input.nextInt();
    }

    public void case1() {
        product.showShowers();
        product.showSinks();
        product.showExteriorPaints();
        product.showInteriorPaints();
        product.showFurniture();
        subMenu();

    }

    public void case2() {
        System.out.println("1.Plumbings\n2.Paints\n3.Furniture");
        switch (input.nextInt()) {
            case (1):
                product.showSinks();
                product.showShowers();
                subMenu();
                break;
            case (2):
                product.showExteriorPaints();
                product.showInteriorPaints();
                subMenu();
                break;
            case (3):
                product.showFurniture();
                subMenu();
                break;
        }
    }

    public void case3() {
        System.out.println("1.Choose from Plumbings\n2.Choose from Paints\n3.Choose from Furniture");
        switch (input.nextInt()) {
            case (1):
                inputIntervals();
                product.showSinksByPrices(from, to);
                product.showShowersByPrices(from, to);
                subMenu();
                break;
            case (2):
                inputIntervals();
                product.showExteriorPaintsByPrices(from, to);
                product.showInteriorPaintsByPrices(from, to);
                subMenu();
                break;
            case (3):
                inputIntervals();
                product.showFurnitureByPrices(from, to);
                subMenu();
                break;
        }
    }

    public void createMenu() {
        switch (input.nextInt()) {
            case (1):
                case1();
                break;
            case (2):
                case2();
                break;
            case (3):
                case3();
                break;

            case (4):
                Runtime.getRuntime().exit(0);
        }

    }
}
